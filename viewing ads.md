```plantuml
@startuml sema
title Просмотр объявления

actor Покупатель
participant "SearchEngine" as M
participant "AdList" as ADL

activate M

Покупатель -> M: showAd(filters)
M -> ADL: getList()
ADL -> M: list
M -> M: sortFilter(filters)
M -> Покупатель: listAd

Покупатель -> M: chooseAd(idAd)
M -> ADL: findById(idAd)
ADL -> M: ad
M -> Покупатель: ad

deactivate M

@enduml
```