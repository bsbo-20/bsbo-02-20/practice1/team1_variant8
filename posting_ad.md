```plantuml
@startuml sema
title Размещение объявления о продаже недвижимости

actor Риелтор
participant "Poster" as M
participant "RealEstate" as RE
participant "RealEstateList" as REL
participant "Ad" as AD
participant "AdList" as ADL

activate M

Риелтор -> M: createAd(infoRE, infoAd)
M -> RE: create(infoRE)
activate RE
RE -> REL: addRealEstate(realEstate)

M -> M: getCurrentUser()
M -> AD: create(infoAd, user)
activate AD
AD -> ADL: addAd(ad)

deactivate M

@enduml
```