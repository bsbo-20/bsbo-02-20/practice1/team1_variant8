```plantuml
@startuml sema
title Оформление сделки

actor Риелтор as R
actor Покупатель as P
participant "Deal" as D
participant "Transaction" as T
participant "TransactionList" as TL
participant "Ad" as A
participant "AdList" as ADL

activate D
activate A

P -> D: buyRealEstate(ad)
D -> D: getCurrentUser()
D -> A: getCost()
A -> D: cost
D -> A: getRealtor()
A -> D: realtor
D -> A: getRealEstate()
A -> D: realEstate
D -> T: create(cost, realtor, buyer, realEstate)
activate T
T -> TL: addTransaction(tran)

R -> D: listTransact()
D -> TL: getList()
TL -> D: list
D -> D: sortForUser()
D -> R: listTL
R -> D: chooseTransact(transaction)
D -> T: setFlag(Boolean)

D -> ADL: deleteAd()

deactivate D

@enduml
```