package Ad;

import java.util.ArrayList;

public class AdList {
    ArrayList<Ad> arrayList;

    public AdList() {
        arrayList = new ArrayList<>();
    }

    public void addAd(Ad ad) {
        arrayList.add(ad);
        System.out.println("Ad has been added to the list!");
    }

    public Ad findById(int id) {
        System.out.println("AdList found ad and returned it");
        return null;
    }

    public ArrayList<Ad> getList() {
        System.out.println("Ad list returned!");
        return arrayList;
    }

    public void deleteAd(Ad ad) {
        System.out.println("Ad has been deleted!");
    }
}
