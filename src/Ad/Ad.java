package Ad;

import RealEstate.RealEstate;
import User.MyUser;

public class Ad {
    private int id;
    private String description;
    private int cost;
    private MyUser realtor;
    private RealEstate realEstate;

    public Ad(int id, String description, int cost, MyUser realtor, RealEstate realEstate) {
        this.id = id;
        this.description = description;
        this.cost = cost;
        this.realtor = realtor;
        this.realEstate = realEstate;
        System.out.println("Ad was created!");
    }

    public RealEstate getRealEstate() {
        System.out.println("Get real estate from Ad.Ad called!");
        return realEstate;
    }

    public int getCost() {
        System.out.println("Get cost from Ad called!");
        return cost;
    }

    public MyUser getRealtor() {
        System.out.println("Get realtor from Ad called!");
        return realtor;
    }


}
