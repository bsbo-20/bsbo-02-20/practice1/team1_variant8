package Transaction;

import java.util.ArrayList;

public class TransactionList {
    ArrayList<Transaction> list;

    public TransactionList() {
        list = new ArrayList<>();
    }

    public void addTransaction(Transaction tran) {
        list.add(tran);
        System.out.println("Transaction has been added to the list!");
    }

    public ArrayList<Transaction> getList() {
        System.out.println("Returned transaction list!");
        return list;
    }


}
