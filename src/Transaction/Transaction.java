package Transaction;

import RealEstate.RealEstate;
import User.MyUser;

import java.util.UUID;

public class Transaction {
    private UUID id;
    private int amount;
    private MyUser realtor;
    private MyUser buyer;
    private RealEstate realEstate;
    private Boolean flag;

    public Transaction(int amount, MyUser realtor, MyUser buyer, RealEstate realEstate) {
        id = UUID.randomUUID();
        this.amount = amount;
        this.realtor = realtor;
        this.buyer = buyer;
        this.realEstate = realEstate;
        flag = false;
        System.out.println("Transaction was created!");
    }

    public int getAmount() {
        System.out.println("Get cost from Transaction called!");
        return amount;
    }

    public MyUser getRealtor() {
        System.out.println("Get realtor from Transaction called!");
        return realtor;
    }

    public MyUser getBuyer() {
        return buyer;
    }

    public RealEstate getRealEstate() {
        System.out.println("Get real estate from Transaction called!");
        return realEstate;
    }

    public void setFlag(Boolean flag) {
        System.out.println("Transaction confirmed!");
        this.flag = flag;
    }
}
