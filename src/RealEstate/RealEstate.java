package RealEstate;

public class RealEstate {
    private int id;
    private String address;
    private double area;
    private TypeRE typeRE;

    public RealEstate(int id, String address, double area, TypeRE typeRE) {
        this.id = id;
        this.address = address;
        this.area = area;
        this.typeRE = typeRE;
        System.out.println("Real estate has been created!");
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public double getArea() {
        return area;
    }

    public TypeRE getTypeRE() {
        return typeRE;
    }
}
