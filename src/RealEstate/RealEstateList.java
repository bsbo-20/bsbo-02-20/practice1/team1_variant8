package RealEstate;

import java.util.ArrayList;

public class RealEstateList {
    ArrayList<RealEstate> arrayList;
    public RealEstateList() {
        arrayList = new ArrayList<>();
    }

    public void addRealEstate(RealEstate realEstate) {
        arrayList.add(realEstate);
        System.out.println("Real estate has been added to the list!");
    }
}
