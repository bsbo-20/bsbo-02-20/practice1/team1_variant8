package User;

public class MyUser {
    private int id;
    private String name;
    private int age;

    public MyUser(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
        System.out.println("User was created");
    }
}
