package Systems;

import Ad.*;
import RealEstate.*;
import User.*;

public class Poster {
    private AdList adList;
    private RealEstateList realEstateList;
    private RealEstate realEstate;
    private MyUser myUser;

    public Poster(MyUser myUser) {
        adList = new AdList();
        realEstateList = new RealEstateList();
        this.myUser = myUser;
    }

    public MyUser getCurrentUser() {
        System.out.println("Get current user called!");
        return myUser;
    }

    public Ad createAd(int idRe, String address, double area, TypeRE typeRE,
                       int id, String description, int cost) {
        System.out.println("Realtor wants post an ad!");
        realEstate = new RealEstate(idRe, address, area, typeRE);
        realEstateList.addRealEstate(realEstate);
        getCurrentUser();
        Ad ad = new Ad(id, description, cost, myUser, realEstate);
        adList.addAd(ad);
        System.out.println("Realtor post ad!");
        return ad;
    }

}
