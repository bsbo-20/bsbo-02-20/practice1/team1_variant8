package Systems;

import Ad.*;
import User.MyUser;
import Transaction.*;

import java.util.ArrayList;

public class Deal {
    private static AdList adList;
    private static TransactionList transactionList;
    private MyUser user;
    private Ad ad;
    private Transaction transaction;
    public Deal(MyUser myUser) {
        this.user = myUser;
        adList = new AdList();
        transactionList = new TransactionList();
    }

    public void buyRealEstate(Ad ad) {
        System.out.println("Buyer wants buy real estate!");
        this.ad = ad;
        getCurrentUser();
        transaction = new Transaction(ad.getCost(), ad.getRealtor(), user, ad.getRealEstate());
        transactionList.addTransaction(transaction);
    }

    public MyUser getCurrentUser() {
        System.out.println("Get current user called!");
        return user;
    }

    public ArrayList<Transaction> listTransact() {
        System.out.println("Call list transact!");
        transactionList.getList();
        sortForUser(null);
        System.out.println("Returned sorted list!");
        return null;
    }

    private void sortForUser(MyUser realtor) {
        System.out.println("Sort transactions for realtor!");
    }

    public void chooseTransact(Transaction transaction) {
        System.out.println("Choose transact called!");
        this.transaction.setFlag(true);
        adList.deleteAd(ad);
    }
}
