import Ad.*;
import RealEstate.*;
import Systems.*;
import User.MyUser;

public class Main {
    public static void main(String[] args) {
        Ad ad;

        System.out.println("Demonstration of the poster:");
        System.out.println("______________________________________________");

        Poster poster = new Poster(new MyUser(1, "Sema", 13));
        ad = poster.createAd(1, "Ramenki 14k2",43.2, TypeRE.Commercial, 1, "Совершенно новая квартирка",
                123456);
        System.out.println("______________________________________________\n\n");
        System.out.println("Demonstration of the search engine:");
        System.out.println("______________________________________________");

        SearchEngine searchEngine = new SearchEngine();
        searchEngine.showAd("Sema");
        searchEngine.chooseAd(4);

        System.out.println("______________________________________________\n\n");
        System.out.println("Demonstration of the deal:");
        System.out.println("______________________________________________");

        Deal deal = new Deal(new MyUser(1, "Sema", 13));
        deal.buyRealEstate(ad);
        deal.listTransact();
        deal.chooseTransact(null);
    }
}