```plantuml
@startuml абоба
scale 2
skinparam ClassAttributeIconSize 0

class Deal
{
	-AdList adList
	-Transaction transaction
	-TransactionList transactionList
	-MyUser user
	-Ad ad
	-getCurrentUser()
	+buyRealEstate(Ad)
	+listTransact()
	-sortForUser()
	chooseTransact(Transaction transaction)
}

class SearchEngine
{
	-List<AdList> list
	+showAd(String)
	+chooseAd(Integer)
	-sortFilter(String)
}

class Poster
{
	-RealEstate realEstate
	-RealEstateList reList
	-AdList adList
	-getCurrentUser()
	+createAd(Integer, String, Double, TypeRE, Integer String, Integer)
}

class MyUser
{
	-Integer id
	-String name
	-Integer age
	+User(Integer, String, Integer)
}

enum TypeRE
{
	Residential
	Commercial
	Public
}

class RealEstate
{
	-Integer id 
	-String address
	-Double area
	-TypeRE typeRE
	+RealEstate(InfoRe)
}

class RealEstateList
{
	-List<RealEstate> list
	+addRealEstate(RealEstate)
}

class Ad
{
	-Integer id
	-String description
	-Integer cost
	-MyUser user
	-RealEstate realEstate
	+Ad(String, String)
	+getRealEstate()
}

class AdList
{
	-List<Ad> list
	+addAd(ad)
	+findById(Integer)
	+getList(String)
	+deleteAd(ad)
}


class Transaction
{
	-UUID id
	-Integer amount
	-MyUser rieltor
	-MyUser buyer
	-RealEstate realEstate
	-Boolean flag
	+Transaction(Integer, MyUser, MyUser, RealEstate)
}

class TransactionList
{
	-List<Transaction> list
	+addTransaction()
}

AdList o-- Ad
RealEstateList o-- RealEstate
TransactionList o-- Transaction

Poster o-- AdList
Poster o-- Ad
Poster o-- RealEstate
Poster o-- RealEstateList
Poster o-- TypeRE

SearchEngine o-- AdList

Deal o-- TransactionList
Deal o-- Transaction
Deal o-- AdList

@enduml
```